/// 
/// @file        CounterPCL.h
/// @author      Eduardo Quesada Díaz <edvqd90@gmail.com>
/// @version     1.0
/// 
/// @section     DESCRIPTION 
/// Class that implements a person counter using data from a Kinect camera
/// using a zenital view.
///

#include "CounterPCL.h"

// Namespaces
using namespace cv;
using namespace std;

///
/// Visualizer for the planar regions extracted using PCL.
///
/// @param cloud Cloud which has had its planar regions separated.
/// @param regions The planar regions calculated from the cloud.
/// @return The viewer with the planar regions.
///
boost::shared_ptr<pcl::visualization::PCLVisualizer> planarRegionsVis(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, std::vector<pcl::PlanarRegion<pcl::PointXYZRGB>, Eigen::aligned_allocator<pcl::PlanarRegion<pcl::PointXYZRGB> > > &regions) 
{
    // Open 3D viewer and add point cloud.
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer with planar regions"));

    // Mix the planar regions with RGB values.
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");

    // A local name will be assigned for easier location and deletion.
    char name[1024];
    unsigned char red [6] = {255, 0, 0, 255, 255, 0};
    unsigned char grn [6] = {0, 255, 0, 255, 0, 255};
    unsigned char blu [6] = {0, 0, 255, 0, 255, 255};

    pcl::PointCloud<pcl::PointXYZRGB>::Ptr contour(new pcl::PointCloud<pcl::PointXYZRGB>);

    float normfactor = 20.5f;
    for (size_t i = 0; i < regions.size(); i++) 
    {
        // Eigenvectors are used to identify regions.
        Eigen::Vector3f centroid = regions[i].getCentroid();
        Eigen::Vector4f planenormal = regions[i].getCoefficients();
        pcl::PointXYZ pt1 = pcl::PointXYZ(centroid[0], centroid[1], centroid[2]);
        pcl::PointXYZ pt2 = pcl::PointXYZ(centroid[0] + (normfactor * planenormal[0]),
                centroid[1] + (normfactor * planenormal[1]),
                centroid[2] + (normfactor * planenormal[2]));

        cout << "normal " << planenormal[0] << " " << planenormal[1] << " " << planenormal[2] << endl;
        sprintf(name, "normal_%d", unsigned (i));
        cout << name << endl;
        viewer->addArrow(pt2, pt1, 1.0, 0, 0, false, name);

        contour->points = regions[i].getContour();
        sprintf(name, "plane_%02d", int (i));

        pcl::PointXYZRGB min_pt, max_pt;
        pcl::getMinMax3D(*contour, min_pt, max_pt);
        cout << "min " << min_pt.x << " " << min_pt.y << " " << min_pt.z << endl;
        cout << "max " << max_pt.x << " " << max_pt.y << " " << max_pt.z << endl;

        cout << name << endl;
        
        pcl::visualization::PointCloudColorHandlerCustom <pcl::PointXYZRGB> color(contour, red[i % 6], grn[i % 6], blu[i % 6]);
        if (!viewer->updatePointCloud(contour, color, name))
            viewer->addPointCloud(contour, color, name);

        // Set initial position for the camera.
        viewer->setCameraPosition(320, 240, -1000, 320, 240, 2000, 0, -1, 0);

        //  et properties for the cloud.
        viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, name);
    }

    return (viewer);
}

/// 
/// Shows a PCL cloud.
///
/// @param cloud The cloud to visualize.
/// @return The viewer with the cloud.
///
boost::shared_ptr<pcl::visualization::PCLVisualizer> simpleVis(pcl::PointCloud<pcl::PointXYZ>::ConstPtr cloud) 
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer simple"));
    viewer->setBackgroundColor(0, 0, 0);
    viewer->addPointCloud<pcl::PointXYZ> (cloud, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
    return (viewer);
}

///
/// Shows a PCL cloud where each point has RGB components.
///
/// @param cloud The cloud to visualize.
/// @return The viewer with the cloud.
///
boost::shared_ptr<pcl::visualization::PCLVisualizer> rgbVis(pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud) 
{
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("3D Viewer"));
    viewer->setBackgroundColor(0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    
    viewer->setCameraPosition(320, 240, -1000, 320, 240, 2000, 0, -1, 0);
    return (viewer);
}

///
/// Shows a PCL cloud where each point has RGB components in a designed viewer.
///
/// @param cloud The cloud to visualize.
/// @param viewer The viewer to show the cloud in.
/// @return The viewer with the cloud.
///
boost::shared_ptr<pcl::visualization::PCLVisualizer> rgbVis (pcl::PointCloud<pcl::PointXYZRGB>::ConstPtr cloud, boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer)
{
    viewer->setBackgroundColor (0, 0, 0);
    pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(cloud);
    viewer->addPointCloud<pcl::PointXYZRGB> (cloud, rgb, "sample cloud");
    viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
    
    viewer->setCameraPosition (320, 240, -1000, 320, 240, 2000, 0, -1, 0); 
    return (viewer);
}
    
///
/// Starts the people counter.
///
/// @param kinect Determines if Kinect is providing the images directly,
/// or if the program have the images on a folder.
/// @param folder In case Kinect is not providing the images, the program 
/// will use the ones on this folder.
///
void CounterPCL::LaunchCounter(bool kinect, char* filelist) 
{
    // Variables to control the Kinect camera.
    //Freenect::Freenect freenect;
    //KinectHandler& device = freenect.createDevice<KinectHandler>(0);
    //VideoCapture * device;
    VideoCapture device(CV_CAP_OPENNI);
    
    // Min and max values of the depth image.
    float minDepth = 1000000;
    float maxDepth = -1000000;
    // List of both RGB and depth image file names.
    char filesDepth[FILES_PER_SESSION][FILE_NAME_LENGTH];
    char filesRGB[FILES_PER_SESSION][FILE_NAME_LENGTH];
    // Total number of files read.
    int nfiles = 0;

    // Viewer for the foreground blob.
    bool viewerFGCreated = false;
    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewerFG;

    // PCL clouds for normal estimation and segmentation.
    pcl::IntegralImageNormalEstimation<pcl::PointXYZRGB, pcl::Normal> normalEstimation;
    pcl::OrganizedMultiPlaneSegmentation<pcl::PointXYZRGB, pcl::Normal, pcl::Label> multiPlaneSeg;
   
    // Variables that keep the current image for processing
    // and the accumulate for the mean.
    cv::Mat imageDepth, imageRGB, accumDepth, accumRGB, mask;

    FILE * fileDepthList;
    
    char fileTmp[2 * FILE_NAME_LENGTH];
    double minValue, maxValue;
    Point minLoc, maxLoc;

    // If we are receiving the images from Kinect, we initialize the stream.
    if (kinect)
    {
        //VideoCapture devTmp(CV_CAP_OPENNI);
        //devTmp.set(CV_CAP_OPENNI_IMAGE_GENERATOR_OUTPUT_MODE, CV_CAP_OPENNI_VGA_30HZ);
        //device = &devTmp;  
        if (!device.isOpened())
        {
            cout << "Could not open a Kinect." << endl;
            return;
        } 
    }
    // If not, we populate a vector with all the file names.
    else
    {
        fileDepthList  = fopen(filelist, "r");
        if (fileDepthList != NULL)
        {
            // Reading of the name files.
            while (fscanf(fileDepthList, "%s\n", fileTmp) != EOF)
            {
                // A filename has been read.
                if (fileTmp != NULL) 
                {                
                    // Copy the filename with the rest of the global path
                    // Upgradeable: directly include the global path 
                    // in the file with the names.
                    sprintf(filesDepth[nfiles], "/media/Data/Proyecto/%s", fileTmp);
                    // Create RGB equivalent to the already depth filename.
                    char *bar = strrchr(filesDepth[nfiles], '/');
                    char *dot = strrchr(filesDepth[nfiles], '.');

                    strncpy(filesRGB[nfiles], filesDepth[nfiles], bar - filesDepth[nfiles] + 1);
                    strncat(filesRGB[nfiles], "RGB", 3);
                    strncat(filesRGB[nfiles], bar + 6, dot - (bar + 6));
                    strncat(filesRGB[nfiles], ".jpg", 4);

                    ++nfiles;
                }
            }
            // Close the file.
            fclose(fileDepthList);
        }
        else
        {
            cout << "File " << filelist << " does not exist." << endl;
        }
    }
        

    // Calculate BG model.
    // We take FRAMES_BG_MODEL frames and accumulate them.
    for (int frame = 0; frame < FRAMES_BG_MODEL; ++frame) 
    {
        // Loading images.
        if (kinect)
        {
            device.grab();
            device.retrieve(imageRGB, CV_CAP_OPENNI_BGR_IMAGE);
            device.retrieve(imageDepth, CV_CAP_OPENNI_DEPTH_MAP);
        }
        else
        {
            imageRGB = cv::imread(filesRGB[frame]);
            imageDepth = cv::imread(filesDepth[frame], CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
        }
        // Calculation of the min and max of the depth image.
        minMaxLoc(imageDepth, &minValue, &maxValue, &minLoc, &maxLoc);
        // Convert images to float.
        imageDepth.convertTo(imageDepth, CV_32F);
        imageRGB.convertTo(imageRGB, CV_32FC3);

        // Accumulation.
        if (frame)  // All frames except the first one.
        {
            // Apply a mask to all zero values.
            imageDepth.convertTo(mask, CV_8U);
            cv::threshold(mask, mask, 0, 255, THRESH_BINARY_INV);
            // Set zeros to max depth.
            imageDepth.setTo(maxValue, mask);
            // Finally, the accumulation.
            cv::accumulate(imageDepth, accumDepth);
            cv::accumulate(imageRGB, accumRGB);
        } 
        else        // Only the first frame.
        {
            // Initialize mask.
            mask = cv::Mat::zeros(accumDepth.rows, accumDepth.cols, CV_8U);
            // Conversion of imageDepth to byte type.
            imageDepth.convertTo(mask, CV_8U);
            cv::threshold(mask, mask, 0, 255, THRESH_BINARY_INV);
            // Set zeros to max depth.
            imageDepth.setTo(maxValue, mask);
            // Initialize accumulators.
            accumDepth = imageDepth.clone();
            accumRGB = imageRGB.clone();
        }
    }       // End accumulator.

    // Finally calculate the mean.
    accumDepth = accumDepth * 1. / FRAMES_BG_MODEL;
    accumRGB = accumRGB * 1. / FRAMES_BG_MODEL;
    // Calculate min and max for background model.
    minMaxLoc(accumDepth, &minValue, &maxValue, &minLoc, &maxLoc);
    // Conversion of accumulated RGB model for later accumulations.
    accumRGB.convertTo(accumRGB, CV_8U);

    // Creation cloud point background model with PCL.
    // Intrinsic Kinect values
    // http://wiki.ros.org/kinect_calibration/technical
    Eigen::Matrix3f kinectIntrinsics;
    kinectIntrinsics << 525, 0.0, 319.5, 0.0, 525, 239.5, 0.0, 0.0, 1.0;
    float xInvertedFocal = 1 / kinectIntrinsics(0, 0);
    float yInvertedFocal = 1 / kinectIntrinsics(1, 1);
    // Background point cloud variable.
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudBG(new pcl::PointCloud<pcl::PointXYZRGB>);

    // If there are images available, we can process them.
    // http://www.pcl-users.org/Conversion-from-cv-Mat-to-pcl-PointCloud-td4023936.html
    // http://stackoverflow.com/questions/14877375/integral-image-normal-estimation-from-kinect-depth-image
    if (!accumDepth.empty() && !accumRGB.empty()) 
    {
        // Initialize point cloud.
        cloudBG->width = accumDepth.cols;
        cloudBG->height = accumDepth.rows;
        cloudBG->resize(cloudBG->width * cloudBG->height);

        // Creation of the point cloud.
        for (size_t i = 0; i < cloudBG->points.size(); ++i) 
        {
            int row = (int) i / accumDepth.cols;
            int col = (int) i % accumDepth.cols;
            // Depth value.
            float depthValue = accumDepth.at<float>(row, col);

            if (depthValue == depthValue) // If depthBG is not NaN
            {
                cloudBG->points[i].x = (col - kinectIntrinsics(0, 2)) * depthValue * xInvertedFocal;
                cloudBG->points[i].y = (row - kinectIntrinsics(1, 2)) * depthValue * yInvertedFocal;

                // Store values.
                if (depthValue == 0) // Invalid values
                {
                    cloudBG->points[i].x = std::numeric_limits<float>::quiet_NaN();
                    cloudBG->points[i].y = std::numeric_limits<float>::quiet_NaN();
                    cloudBG->points[i].z = std::numeric_limits<float>::quiet_NaN();
                    cloudBG->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                    cloudBG->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                    cloudBG->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();
                } 
                else                // Valid values
                {
                    // Update min and max values.
                    if (depthValue > maxDepth)
                    {
                        maxDepth = depthValue;
                    }
                    if (depthValue < minDepth)
                    {
                        minDepth = depthValue;
                    }
                    cloudBG->points[i].z = depthValue;
                    cloudBG->points[i].r = accumRGB.at<cv::Vec3b>(row, col)[2];
                    cloudBG->points[i].g = accumRGB.at<cv::Vec3b>(row, col)[1];
                    cloudBG->points[i].b = accumRGB.at<cv::Vec3b>(row, col)[0];
                }
            } else                  // If depthValue is NaN
            {
                cloudBG->points[i].x = std::numeric_limits<float>::quiet_NaN();
                cloudBG->points[i].y = std::numeric_limits<float>::quiet_NaN();
                cloudBG->points[i].z = std::numeric_limits<float>::quiet_NaN();
                cloudBG->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                cloudBG->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                cloudBG->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();
            }
        }
        // Redefine the dimensions for treating the matrix as organized.
        cloudBG->width = accumDepth.cols;
        cloudBG->height = accumDepth.rows;
        cloudBG->resize(cloudBG->width * cloudBG->height);

        // End of the creation of the background point cloud.


        // Conversion from PointXYZRGB to PointXYZ for visualization purposes.
        pcl::PointCloud<pcl::PointXYZ>::Ptr cloudBGXYZ(new pcl::PointCloud<pcl::PointXYZ>());
        pcl::copyPointCloud(*cloudBG, *cloudBGXYZ);
#if DEBUG
        // Visualize this cloud.
        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer;
        viewer = simpleVis(cloudBGXYZ);
        while (!viewer->wasStopped())
        {
            viewer->spinOnce(100);
            cvWaitKey(1);
        }
        // Visualize background cloud each point with its color.
        viewer = rgbVis(cloudBG);
        while (!viewer->wasStopped()) 
        {
            viewer->spinOnce(100);
            cvWaitKey(1);
        }
#endif


        // Calculation of the normals of the walls.
        // Source: organized_segmentation_demo.cpp
        pcl::PointCloud<pcl::Normal>::Ptr normalCloud(new pcl::PointCloud<pcl::Normal>);
        normalEstimation.setInputCloud(cloudBG);
        normalEstimation.setNormalEstimationMethod(normalEstimation.AVERAGE_3D_GRADIENT);
        normalEstimation.setMaxDepthChangeFactor(0.02f);
        normalEstimation.setNormalSmoothingSize(10.0f);
        normalEstimation.compute(*normalCloud);

        // Normals calculated --- Segmenting planes

        multiPlaneSeg.setInputNormals(normalCloud);
        multiPlaneSeg.setInputCloud(cloudBG);
        std::vector<pcl::PlanarRegion<pcl::PointXYZRGB>, Eigen::aligned_allocator<pcl::PlanarRegion<pcl::PointXYZRGB> > > regions;
        std::vector<pcl::ModelCoefficients> modelCoefficients;
        std::vector<pcl::PointIndices> inlierIndices;
        pcl::PointCloud<pcl::Label>::Ptr labels(new pcl::PointCloud<pcl::Label>);
        std::vector<pcl::PointIndices> labelIndices;
        std::vector<pcl::PointIndices> boundaryIndices;

        if (USE_PLANAR_REFINEMENT) 
        {
            multiPlaneSeg.segmentAndRefine(regions, modelCoefficients, 
                            inlierIndices, labels, labelIndices, boundaryIndices);
        } 
        else 
        {
            multiPlaneSeg.segment(regions);
        }

#if DEBUG
        // Show planes.
        viewer = planarRegionsVis(cloudBG, regions);
        while (!viewer->wasStopped()) 
        {
            viewer->spinOnce(100);
            cvWaitKey(1);
        }
#endif
        // Delete planes.
        // https://github.com/wg-perception/object_tracker/blob/master/include/PointCloudUtils.hpp
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPlanesRemoved(new pcl::PointCloud<pcl::PointXYZRGB>);
        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudPlanesZeroed(new pcl::PointCloud<pcl::PointXYZRGB>);
        boost::shared_ptr<std::vector<int> > inliers(new std::vector<int>());
        for (unsigned int i = 0; i < inlierIndices.size(); ++i) 
        {
            inliers->insert(inliers->end(), inlierIndices[i].indices.begin(), inlierIndices[i].indices.end());
        }
        pcl::ExtractIndices<pcl::PointXYZRGB> extractIndices;
        extractIndices.setInputCloud(cloudBG);
        extractIndices.setIndices(inliers);
        extractIndices.setNegative(true);
        extractIndices.filter(*cloudPlanesRemoved);

#if DEBUG
        // Show background without removed planes.
        viewer = rgbVis(cloudPlanesRemoved);
        while (!viewer->wasStopped()) 
        {
            viewer->spinOnce(100);
            cvWaitKey(1);
        }
#endif
        
        // Zeroing localized planes.
        pcl::PassThrough<pcl::PointXYZRGB> filter;
        filter.setInputCloud(cloudBG);
        filter.setIndices(inliers);
        filter.setKeepOrganized(true);
        filter.setUserFilterValue(0.f);
        filter.filter(*cloudPlanesZeroed);
        // End delete planes.

        // Background sustraction.
        // We compare each frame with the background model.
        int nframe = 0;
        long nVisPixels = 0;
        cv::Mat maskFG;

        // Variables for people counting.
        cv::Mat lastFrame = cv::Mat::zeros(accumDepth.size(), CV_8U);
        PeopleClass people;

        // For each fotogram.
        while (kinect?(nframe < FILES_PER_SESSION):(nframe < nfiles)) 
        {
            minDepth = 1000000;
            maxDepth = -1000000;
            nVisPixels = 0;

            // Stores people info on this frame.
            PeopleClass peopleCurrentFrame;

            // Loading of RGB and depth images.
            if (kinect)
            {
                device.grab();
                device.retrieve(imageRGB, CV_CAP_OPENNI_BGR_IMAGE);
                device.retrieve(imageDepth, CV_CAP_OPENNI_DEPTH_MAP);
            }
            else
            {
                imageRGB = cv::imread(filesRGB[nframe]);
                imageDepth = cv::imread(filesDepth[nframe], CV_LOAD_IMAGE_ANYDEPTH | CV_LOAD_IMAGE_ANYCOLOR);
            }
            maskFG = cv::Mat::zeros(imageRGB.rows, imageRGB.cols, CV_8U);

#if DEBUG || ARFF
            if (!kinect)
            {
                cout << filesRGB[nframe] << endl;
            }
#endif

            // New cloud point variables.
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudFG(new pcl::PointCloud<pcl::PointXYZRGB>);

            // Mask for counting people.
            cv::Mat binaryMask = cv::Mat::zeros(accumDepth.size(), CV_8U);
            // We calculate the contours for later use.
            CvSeq * chainMask = 0;
            CvMemStorage * storageMask = cvCreateMemStorage(0);
            IplImage * lastFrameAux = &IplImage(lastFrame);
            cvFindContours(lastFrameAux, storageMask, &chainMask,
                    sizeof (CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

            // Conversion to point cloud only if both images are available.
            if (!imageDepth.empty() && !imageRGB.empty()) 
            {
                // Convert to 32 bit float.
                imageDepth.convertTo(imageDepth, CV_32F);

                // Obtaining of the point cloud from RGB and depth info.
                // http://www.pcl-users.org/Conversion-from-cv-Mat-to-pcl-PointCloud-td4023936.html
                // http://stackoverflow.com/questions/14877375/integral-image-normal-estimation-from-kinect-depth-image
                cloud->width = imageDepth.cols;
                cloud->height = imageDepth.rows;
                cloud->resize(cloud->width * cloud->height);

                cloudFG->width = imageDepth.cols;
                cloudFG->height = imageDepth.rows;
                cloudFG->resize(cloudFG->width * cloudFG->height);

                // Creation of both point clouds.
                for (size_t i = 0; i < cloud->points.size(); ++i) 
                {
                    int row = (int) i / imageDepth.cols;
                    int col = (int) i % imageDepth.cols;
                    float depthValue = imageDepth.at<float>(row, col);
                    float depthValueBG = accumDepth.at<float>(row, col);

                    if (depthValue == depthValue)   // Not NaN
                    {
                        cloud->points[i].x = (col - kinectIntrinsics(0, 2)) * depthValue * xInvertedFocal;
                        cloud->points[i].y = (row - kinectIntrinsics(1, 2)) * depthValue * yInvertedFocal;

                        // Store min and max values.
                        if (depthValue != 0)
                        {
                            if (depthValue > maxDepth)
                            {
                                maxDepth = depthValue;
                            }
                            if (depthValue < minDepth)
                            {
                                minDepth = depthValue;
                            }
                        }

                        if (depthValue == 0 && 0)   // Invalid values
                        {
                            cloud->points[i].z = std::numeric_limits<float>::quiet_NaN();
                            cloud->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                            cloud->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                            cloud->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();

                            cloudFG->points[i].z = std::numeric_limits<float>::quiet_NaN();
                            cloudFG->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                            cloudFG->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                            cloudFG->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();
                        } 
                        else 
                        {
                            cloud->points[i].z = depthValue;
                            cloud->points[i].r = imageRGB.at<cv::Vec3b>(row, col)[2];
                            cloud->points[i].g = imageRGB.at<cv::Vec3b>(row, col)[1];
                            cloud->points[i].b = imageRGB.at<cv::Vec3b>(row, col)[0];

                            // If it is close enough, we add it to the foreground image.
                            if (depthValue < depthValueBG - 100 && depthValue > 0 && depthValueBG > 0) 
                            {
                                cloudFG->points[i].x = (col - kinectIntrinsics(0, 2)) * depthValue * xInvertedFocal;
                                cloudFG->points[i].y = (row - kinectIntrinsics(1, 2)) * depthValue * yInvertedFocal;
                                cloudFG->points[i].z = depthValue;
                                cloudFG->points[i].r = imageRGB.at<cv::Vec3b>(row, col)[2];
                                cloudFG->points[i].g = imageRGB.at<cv::Vec3b>(row, col)[1];
                                cloudFG->points[i].b = imageRGB.at<cv::Vec3b>(row, col)[0];
                                maskFG.at<uchar>(row, col) = 255;
                                nVisPixels++;
                            } 
                            else 
                            {
                                cloudFG->points[i].x = (col - kinectIntrinsics(0, 2)) * depthValue * xInvertedFocal;
                                cloudFG->points[i].y = (row - kinectIntrinsics(1, 2)) * depthValue * yInvertedFocal;
                                cloudFG->points[i].z = depthValue * 3;
                                cloudFG->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                                cloudFG->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                                cloudFG->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();
                            }
                        }
                    } 
                    else                    // NaN case
                    {
                        cloud->points[i].x = std::numeric_limits<float>::quiet_NaN();
                        cloud->points[i].y = std::numeric_limits<float>::quiet_NaN();
                        cloud->points[i].z = std::numeric_limits<float>::quiet_NaN();
                        cloud->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                        cloud->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                        cloud->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();

                        cloudFG->points[i].x = std::numeric_limits<float>::quiet_NaN();
                        cloudFG->points[i].y = std::numeric_limits<float>::quiet_NaN();
                        cloudFG->points[i].z = std::numeric_limits<float>::quiet_NaN();
                        cloudFG->points[i].r = std::numeric_limits<unsigned char>::quiet_NaN();
                        cloudFG->points[i].g = std::numeric_limits<unsigned char>::quiet_NaN();
                        cloudFG->points[i].b = std::numeric_limits<unsigned char>::quiet_NaN();
                    }
                }
                cloud->width = imageDepth.cols;
                cloud->height = imageDepth.rows;
                cloud->resize(cloud->width * cloud->height);

                cloudFG->width = imageDepth.cols;
                cloudFG->height = imageDepth.rows;
                cloudFG->resize(cloudFG->width * cloudFG->height);
                // End of creation of point clouds.

                // If there are a minimum number of pixels on the foreground
                // there is something worthwhile on it.
                if (nVisPixels > MIN_FG_PIXELS) 
                {
                    // Calculate normals of foreground point cloud.
                    pcl::PointCloud<pcl::Normal>::Ptr normalCloudFG(new pcl::PointCloud<pcl::Normal>);
                    normalEstimation.setInputCloud(cloudFG);
                    normalEstimation.setNormalEstimationMethod(normalEstimation.AVERAGE_3D_GRADIENT);
                    normalEstimation.setMaxDepthChangeFactor(0.02f);
                    normalEstimation.setNormalSmoothingSize(10.0f);
                    normalEstimation.compute(*normalCloudFG);

                    // Blob separation.
                    // Obtaining the blob contours.
                    cv::Mat maskBlob = cv::Mat::zeros(accumDepth.size(), CV_8U);
                    CvSeq * chain = 0;
                    CvMemStorage * storage = cvCreateMemStorage(0);
                    double area = 0;
                    IplImage * iplh = &IplImage(maskFG);
                    // Stores the blob for the head.
                    cv::Mat iplHead = cv::Mat::zeros(accumDepth.size(), CV_8U);
                    cv::Mat headsBinary = cv::Mat::zeros(accumDepth.size(), CV_8U);

                    bool firstContour = false;
                    int bigBlobs = 0;

                    // Searches for contours on the foreground mask.
                    // Caution: cvFindContours modifies input image.
                    int numContours = cvFindContours(iplh, storage, &chain,
                            sizeof (CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

                    // There are contours on the foreground.
                    if (chain != 0) 
                    {
                        // Iteration for all the contours found.
                        for (CvSeq * contour = chain; contour != 0; contour = contour->h_next) 
                        {
                            // We mantain a mask for the already detected people.
                            cv::Mat detectedMask = cv::Mat::zeros(accumDepth.size(), CV_8U);
                            bool allDetected = false;
                            // Calculation of the contour internal area.
                            area = fabs(cvContourArea(contour));
                            // If it is big enough
                            if (area > (iplh->height * iplh->width * 0.01)
                                    && area < (iplh->height * iplh->width * 0.6)) 
                            {
                                // Calculation of the contour container.
                                CvRect rectTmp = cvBoundingRect(contour);

                                if (!firstContour || area > MIN_FG_PIXELS) 
                                {
                                    firstContour = true;
                                    // We iterate the same contour until all heads
                                    // have been detected.
                                    int iterations = 0;
                                    while (!allDetected) 
                                    {
                                        ++iterations;
                                        // Initialize the image that will contain the head.
                                        iplHead = cv::Mat::zeros(accumDepth.size(), CV_8U);
                                        // Count the number of big blobs found.
                                        bigBlobs++;
                                        // Blob characteristics
                                        IplImage iplhAux = maskBlob;
                                        // Draw the contour filled up, and serves as a mask.
                                        cvDrawContours(&iplhAux, contour, CV_RGB(255, 255, 255),
                                                CV_RGB(255, 255, 255), 0, CV_FILLED);
                                        // Elements on the blob mask.
                                        int blobElements = countNonZero(maskBlob);

                                        cv::Mat haux = Mat::zeros(accumDepth.size(), accumDepth.channels());
                                        // Mask average depth.
                                        bitwise_and(accumDepth, accumDepth, haux, maskBlob);

                                        Scalar suma = sum(haux);
                                        float floorDistance;
                                        int individualHeight = 0;

                                        // Obtaining the distance to the floor.
                                        int numFloorPoints = 0;
                                        for (int i = rectTmp.y; i < rectTmp.y + rectTmp.height; ++i) 
                                        {
                                            for (int j = rectTmp.x; j < rectTmp.x + rectTmp.width; ++j) 
                                            {
                                                // Pixel in the rect is from the blob we are interested in.
                                                if (maskBlob.data[maskBlob.channels() * (maskBlob.cols * i + j)] != 0
                                                        && cloudPlanesZeroed->points[(cloudPlanesZeroed->width * i + j)].z != 0
                                                        && detectedMask.data[detectedMask.cols * i + j] == 0) 
                                                {
                                                    ++numFloorPoints;
                                                    floorDistance += cloudPlanesZeroed->points[(cloudPlanesZeroed->width * i + j)].z;
                                                }
                                            }
                                        }
                                        if (numFloorPoints) 
                                        {
                                            floorDistance /= (float) numFloorPoints;
                                        } 
                                        else 
                                        {
                                            floorDistance = suma.val[0] / blobElements;
                                        }

                                        // Obtaining of the max height of the blob.
                                        for (int i = rectTmp.y; i < rectTmp.y + rectTmp.height; ++i) 
                                        {
                                            for (int j = rectTmp.x; j < rectTmp.x + rectTmp.width; ++j) 
                                            {
                                                if (maskBlob.data[maskBlob.channels() * (maskBlob.cols * i + j)] != 0
                                                        && detectedMask.data[detectedMask.cols * i + j] == 0) 
                                                {
                                                    // If it is the highest point, update blob height.
                                                    if (individualHeight < floorDistance - cloudFG->points[i * cloudFG->width + j].z
                                                            && cloudFG->points[i * cloudFG->width + j].z > 0) 
                                                    {
                                                        individualHeight = floorDistance - cloudFG->points[i * cloudFG->width + j].z;
                                                    }
                                                }
                                            }
                                        }

                                        // Obtain centroid from highest area of the blob.
                                        int nHighArea = 0;
                                        int centroidx = 0;
                                        int centroidy = 0;

                                        pcl::PointCloud<pcl::PointXYZRGB>::Ptr headCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
                                        headCloud->width = iplHead.cols;
                                        headCloud->height = iplHead.rows;
                                        headCloud->resize(headCloud->width * headCloud->height);

                                        for (int i = rectTmp.y; i < rectTmp.y + rectTmp.height; ++i) 
                                        {
                                            for (int j = rectTmp.x; j < rectTmp.x + rectTmp.width; ++j) 
                                            {
                                                if (maskBlob.data[maskBlob.channels() * (maskBlob.cols * i + j)] != 0
                                                        && detectedMask.data[detectedMask.cols * i + j] == 0) 
                                                {
                                                    float dpth = cloudFG->points[i * cloudFG->width + j].z;
                                                    if (floorDistance - dpth > individualHeight * HEAD_HEIGHT && dpth > 0) 
                                                    {
                                                        ++nHighArea;
                                                        centroidx += j;
                                                        centroidy += i;
                                                        // A cloud with only the points corresponding to the head is created.
                                                        iplHead.data[i * iplHead.cols + j] = 255;
                                                        headCloud->points[i * iplHead.cols + j].x = cloudFG->points[i * iplHead.cols + j].x;
                                                        headCloud->points[i * iplHead.cols + j].y = cloudFG->points[i * iplHead.cols + j].y;
                                                        headCloud->points[i * iplHead.cols + j].z = cloudFG->points[i * iplHead.cols + j].z;
                                                        headCloud->points[i * iplHead.cols + j].r = cloudFG->points[i * iplHead.cols + j].r;
                                                        headCloud->points[i * iplHead.cols + j].g = cloudFG->points[i * iplHead.cols + j].g;
                                                        headCloud->points[i * iplHead.cols + j].b = cloudFG->points[i * iplHead.cols + j].b;
                                                    }
                                                }
                                            }
                                        }

                                        if (nHighArea != 0)
                                        {
                                            centroidx /= nHighArea;
                                            centroidy /= nHighArea;
                                        }


                                        // Draw region of interest.
#if DEBUG
                                        line(headsBinary, Point(0, headsBinary.rows * PERCENTAGE_ROI),
                                                Point(headsBinary.cols, headsBinary.rows * PERCENTAGE_ROI), 255);
                                        line(headsBinary, Point(0, headsBinary.rows * (1 - PERCENTAGE_ROI)),
                                                Point(headsBinary.cols, headsBinary.rows * (1 - PERCENTAGE_ROI)), 255);

                                        line(headsBinary, Point(0, headsBinary.rows * MARGIN_ROI),
                                                Point(headsBinary.cols, headsBinary.rows * MARGIN_ROI), 200);
                                        line(headsBinary, Point(0, headsBinary.rows * (1 - MARGIN_ROI)),
                                                Point(headsBinary.cols, headsBinary.rows * (1 - MARGIN_ROI)), 200);
#endif
                                        
                                        line(imageRGB, Point(0, headsBinary.rows * PERCENTAGE_ROI),
                                                Point(headsBinary.cols, headsBinary.rows * PERCENTAGE_ROI), 255);
                                        line(imageRGB, Point(0, headsBinary.rows * (1 - PERCENTAGE_ROI)),
                                                Point(headsBinary.cols, headsBinary.rows * (1 - PERCENTAGE_ROI)), 255);

                                        line(imageRGB, Point(0, headsBinary.rows * MARGIN_ROI),
                                                Point(headsBinary.cols, headsBinary.rows * MARGIN_ROI), 200);
                                        line(imageRGB, Point(0, headsBinary.rows * (1 - MARGIN_ROI)),
                                                Point(headsBinary.cols, headsBinary.rows * (1 - MARGIN_ROI)), 200);

                                        // If the blob is in the ROI, we decide if it is a head.
                                        if (centroidy > headsBinary.rows * PERCENTAGE_ROI &&
                                                centroidy < headsBinary.rows * (1 - PERCENTAGE_ROI)) 
                                        {
                                            // Calculation of the circularity.
                                            // Apply closing procedure to smooth the head blob.
                                            Mat element = getStructuringElement(MORPH_ELLIPSE,
                                                    Size(2 * KERNEL_SIZE + 1, 2 * KERNEL_SIZE + 1),
                                                    Point(KERNEL_SIZE, KERNEL_SIZE));
                                            cv::Mat iplHeadClosed = cv::Mat::zeros(accumDepth.size(), CV_8U);
                                            morphologyEx(iplHead, iplHeadClosed, MORPH_CLOSE, element);

                                            // We dilate the image in order to add it to a mask.
                                            cv::Mat iplHeadDilated = cv::Mat::zeros(accumDepth.size(), CV_8U);
                                            dilate(iplHeadClosed, iplHeadDilated, element);
                                            detectedMask += iplHeadDilated;

                                            // Now we calculate the area and perimeter of the head.
                                            CvSeq * chainHead = 0;
                                            CvMemStorage * storageHead = cvCreateMemStorage(0);
                                            IplImage * iplHeadAux = &IplImage(iplHeadClosed);
                                            cvFindContours(iplHeadAux, storageHead, &chainHead,
                                                    sizeof (CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                                            int nCont = 0, noHeads = 0;

                                            // Calculation of the contour internal area and perimeter.
                                            for (CvSeq * cAux = chainHead; cAux != 0; cAux = cAux->h_next, ++nCont) 
                                            {
                                                double areaHead = fabs(cvContourArea(cAux));
                                                double perimeterHead = fabs(cvArcLength(cAux));
                                                CvRect rectHead = cvBoundingRect(cAux);
                                                int biggestHeadNormalX = 0;
                                                int biggestHeadNormalY = 0;
                                                int biggestHeadNormalZ = 0;

                                                cv::Mat oneHeadMask = cv::Mat::zeros(accumDepth.size(), CV_8U);

                                                if (areaHead > (iplh->height * iplh->width * 0.01) && 
                                                    areaHead < (iplh->height * iplh->width * 0.6)) 
                                                {
                                                    // Creation of a sub cloud with only one head
                                                    // this time for sure, as we are using the contour directly.
                                                    pcl::PointCloud<pcl::PointXYZRGB>::Ptr oneHeadCloud(new pcl::PointCloud<pcl::PointXYZRGB>());
                                                    oneHeadCloud->width = iplHead.cols;
                                                    oneHeadCloud->height = iplHead.rows;
                                                    oneHeadCloud->resize(oneHeadCloud->width * oneHeadCloud->height);

                                                    for (int i = rectHead.y; i < rectHead.y + rectHead.height; ++i) 
                                                    {
                                                        for (int j = rectHead.x; j < rectHead.x + rectHead.width; ++j) 
                                                        {
                                                            oneHeadCloud->points[i * iplHead.cols + j].x = headCloud->points[i * iplHead.cols + j].x;
                                                            oneHeadCloud->points[i * iplHead.cols + j].y = headCloud->points[i * iplHead.cols + j].y;
                                                            oneHeadCloud->points[i * iplHead.cols + j].z = headCloud->points[i * iplHead.cols + j].z;
                                                            oneHeadCloud->points[i * iplHead.cols + j].r = headCloud->points[i * iplHead.cols + j].r;
                                                            oneHeadCloud->points[i * iplHead.cols + j].g = headCloud->points[i * iplHead.cols + j].g;
                                                            oneHeadCloud->points[i * iplHead.cols + j].b = headCloud->points[i * iplHead.cols + j].b;
                                                            if (iplHeadClosed.data[i * iplHeadClosed.cols + j] > 0)
                                                            {
                                                                oneHeadMask.data[i * oneHeadMask.cols + j] = 255;
                                                            }
                                                        }
                                                    }

                                                    // We calculate normals for each point and then count
                                                    // how many points have one of the components as its biggest.
                                                    pcl::PointCloud<pcl::Normal>::Ptr headNormalCloud(new pcl::PointCloud<pcl::Normal>);
                                                    normalEstimation.setInputCloud(oneHeadCloud);
                                                    normalEstimation.setNormalEstimationMethod(normalEstimation.AVERAGE_3D_GRADIENT);
                                                    normalEstimation.setMaxDepthChangeFactor(0.02f);
                                                    normalEstimation.setNormalSmoothingSize(10.0f);
                                                    normalEstimation.compute(*headNormalCloud);

                                                    for (size_t k = 0; k < headNormalCloud->points.size(); ++k) 
                                                    {
                                                        if (fabs(headNormalCloud->points[k].normal_x) > fabs(headNormalCloud->points[k].normal_y) &&
                                                                fabs(headNormalCloud->points[k].normal_x) > fabs(headNormalCloud->points[k].normal_z)) 
                                                        {
                                                            ++biggestHeadNormalX;
                                                        }
                                                        if (fabs(headNormalCloud->points[k].normal_y) > fabs(headNormalCloud->points[k].normal_x) &&
                                                                fabs(headNormalCloud->points[k].normal_y) > fabs(headNormalCloud->points[k].normal_z)) 
                                                        {
                                                            ++biggestHeadNormalY;
                                                        }
                                                        if (fabs(headNormalCloud->points[k].normal_z) > fabs(headNormalCloud->points[k].normal_x) &&
                                                                fabs(headNormalCloud->points[k].normal_z) > fabs(headNormalCloud->points[k].normal_y)) 
                                                        {
                                                            ++biggestHeadNormalZ;
                                                        }
                                                    }
                                                }
                                                centroidx = rectHead.x + (rectHead.width / 2.);
                                                centroidy = rectHead.y + (rectHead.height / 2.);

                                                // Finally, calculate the circularity.
                                                double circularity = (FOUR_PI * areaHead) /
                                                        (perimeterHead * perimeterHead);
                                                double normals = biggestHeadNormalZ /
                                                        (double) (biggestHeadNormalX + biggestHeadNormalY + biggestHeadNormalZ);

                                                // Is the person entering or leaving the ROI?
                                                bool tUp = false, tDown = false, tLeft = false, tRight = false;
                                                if (rectHead.y <= MARGIN_ROI * headsBinary.rows)
                                                {
                                                    tUp = true;
                                                }
                                                if (rectHead.y + rectHead.height >= (1 - MARGIN_ROI) * headsBinary.rows)
                                                {
                                                    tDown = true;
                                                }
                                                if (0 >= rectHead.x)
                                                {
                                                    tLeft = true;
                                                }
                                                // The -60 is the difference in resolution between the RGB image and the depth one.
                                                if (headsBinary.cols - 60 <= rectHead.x + rectHead.width)
                                                {
                                                    tRight = true;
                                                }

                                                bool isHead = false;

                                                // We check if the head is in the ROI now.
                                                if (centroidy > headsBinary.rows * PERCENTAGE_ROI && 
                                                    centroidy < headsBinary.rows * (1 - PERCENTAGE_ROI) &&
                                                    areaHead > (iplh->height * iplh->width * 0.01) && 
                                                    areaHead < (iplh->height * iplh->width * 0.6))
                                                {
                                                    // And then, we check if it is a possible person.
                                                    if (((normals * NORMALS_WEIGHT + circularity * (1 - NORMALS_WEIGHT)) > OK_VALUE) && 
                                                          normals > MIN_NORMAL_VALUE && 
                                                          circularity > MIN_CIRC_VALUE) 
                                                    {
                                                        binaryMask += oneHeadMask;
                                                        headsBinary += oneHeadMask;
                                                        isHead = true;
                                                        /*
                                                        rectangle(imageRGB, Point(rectHead.x, rectHead.y),
                                                                Point(rectHead.x+rectHead.width, rectHead.y+rectHead.height), 255);
                                                        rectangle(imageRGB, Point(centroidx - 5, centroidy - 5),
                                                                Point(centroidx + 5, centroidy + 5), 0);
                                                        */
#if DEBUG
                                                        rectangle(headsBinary, Point(rectHead.x, rectHead.y),
                                                                Point(rectHead.x+rectHead.width, rectHead.y+rectHead.height), 255);
                                                        rectangle(headsBinary, Point(centroidx - 5, centroidy - 5),
                                                                Point(centroidx + 5, centroidy + 5), 0);
                                                        cout << "-Norm " << normals << "\t"; //endl;
                                                        cout << "-Circ " << circularity << "\t"; //endl;
                                                        cout << "----- " << normals * NORMALS_WEIGHT + circularity * (1 - NORMALS_WEIGHT) << endl;
#endif          

                                                        // We check if this person was on the last frame.
                                                        cv::Mat andImage = cv::Mat::zeros(accumDepth.size(), CV_8U);
                                                        andImage = 255 * (oneHeadMask & lastFrame);
                                                        IplImage * andImageAux = &IplImage(andImage);
                                                        cvFindContours(andImageAux, storageHead, &chainHead,
                                                                sizeof (CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                                                        // If we have a contour, there is an overlapping
                                                        // between this frame and the last one.
                                                        if (chainHead != 0)
                                                        {
                                                            CvRect andRect = cvBoundingRect(chainHead);
                                                            int areaRect = andRect.width * andRect.height;
                                                            // In the rare case there are multiple contours on the and image
                                                            // we select the biggest one (the others are noise).
                                                            for (CvSeq * cAux = chainHead->h_next; cAux != 0; cAux = cAux->h_next) 
                                                            {
                                                                CvRect auxRect = cvBoundingRect(cAux);
                                                                int area = auxRect.width * auxRect.height;
                                                                if (area > areaRect)
                                                                {
                                                                    areaRect = area;
                                                                    andRect = auxRect;
                                                                }
                                                            }
                                                            // We calculate where was the person on the last frame.
                                                            int centRectX = andRect.x + (andRect.width / 2.);
                                                            int centRectY = andRect.y + (andRect.height / 2.);
                                                            for (CvSeq * cm = chainMask; cm != 0; cm = cm->h_next) 
                                                            {
                                                                CvRect rectCont = cvBoundingRect(cm);
                                                                if (centRectX < (rectCont.x + rectCont.width)
                                                                        && centRectX > rectCont.x
                                                                        && centRectY < (rectCont.y + rectCont.height)
                                                                        && centRectY > rectCont.y)
                                                                {
                                                                    // And then update its position.
#if ARFF
                                                                    cout << individualHeight << "," << normals << "," << circularity << "," << areaHead << ",YES,\t(" << centroidx << " , " << centroidy << ")" << endl;
#endif
                                                                    peopleCurrentFrame.addPerson(centroidx, centroidy, 
                                                                            rectCont.x + (rectCont.width / 2.), rectCont.y + (rectCont.height / 2.),
                                                                            individualHeight, areaHead, areaRect, circularity, normals, tUp, tDown, tLeft, tRight);
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                        // If not, this is a new person.
                                                        else
                                                        {
#if ARFF
                                                            cout << individualHeight << "," << normals << "," << circularity << "," << areaHead << ",YES,\t(" << centroidx << " , " << centroidy << ")" << endl;
#endif
                                                            peopleCurrentFrame.addPerson(centroidx, centroidy, 0, 0, individualHeight, 
                                                                    areaHead, 0, circularity, normals, tUp, tDown, tLeft, tRight);
                                                        }

                                                    } 
                                                    else 
                                                    {
                                                        // Only if it's the first iteration, we add what could be
                                                        // a "strange head" to the heads image.
                                                        if (iterations == 1 && circularity > 0.42)
                                                        {
                                                            binaryMask += oneHeadMask;
                                                            headsBinary += oneHeadMask;
                                                            /*
                                                            rectangle(imageRGB, Point(rectHead.x, rectHead.y),
                                                                    Point(rectHead.x+rectHead.width, rectHead.y+rectHead.height), 255);
                                                            rectangle(imageRGB, Point(centroidx - 5, centroidy - 5),
                                                                    Point(centroidx + 5, centroidy + 5), 0, -1, 8, 0);
                                                            rectangle(imageRGB, Point(centroidx - 6, centroidy - 6),
                                                                    Point(centroidx + 6, centroidy + 6), 255);
                                                            */
#if DEBUG                                                   
                                                            cout << "-Norm " << normals << "\t"; //endl;
                                                            cout << "-Circ " << circularity << "\t"; //endl;
                                                            cout << "----- " << normals * NORMALS_WEIGHT + circularity * (1 - NORMALS_WEIGHT) << endl;
                                                            rectangle(headsBinary, Point(rectHead.x, rectHead.y),
                                                                    Point(rectHead.x+rectHead.width, rectHead.y+rectHead.height), 255);
                                                            rectangle(headsBinary, Point(centroidx - 5, centroidy - 5),
                                                                    Point(centroidx + 5, centroidy + 5), 0, -1, 8, 0);
                                                            rectangle(headsBinary, Point(centroidx - 6, centroidy - 6),
                                                                    Point(centroidx + 6, centroidy + 6), 255);
#endif

                                                            // We check if this possible person was on the last frame.
                                                            cv::Mat andImage = cv::Mat::zeros(accumDepth.size(), CV_8U);
                                                            andImage = 255 * (oneHeadMask & lastFrame);
                                                            IplImage * andImageAux = &IplImage(andImage);
                                                            cvFindContours(andImageAux, storageHead, &chainHead,
                                                                    sizeof (CvContour), CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);
                                                            // If we have a contour, there is an overlapping
                                                            // between this frame and the last one.
                                                            if (chainHead != 0)
                                                            {
                                                                CvRect andRect = cvBoundingRect(chainHead);
                                                                int areaRect = andRect.width * andRect.height;
                                                                // In the rare case there are multiple contours
                                                                // we select the biggest one (the others are noise).
                                                                for (CvSeq * cAux = chainHead->h_next; cAux != 0; cAux = cAux->h_next) 
                                                                {
                                                                    CvRect auxRect = cvBoundingRect(cAux);
                                                                    int area = auxRect.width * auxRect.height;
                                                                    if (area > areaRect)
                                                                    {
                                                                        areaRect = area;
                                                                        chainHead = cAux;
                                                                        andRect = auxRect;
                                                                    }
                                                                }
                                                                // We calculate where was the person on the last frame.
                                                                int centRectX = andRect.x + (andRect.width / 2.);
                                                                int centRectY = andRect.y + (andRect.height / 2.);
                                                                for (CvSeq * cm = chainMask; cm != 0; cm = cm->h_next) 
                                                                {
                                                                    CvRect rectCont = cvBoundingRect(cm);
                                                                    if (centRectX < rectCont.x + rectCont.width
                                                                            && centRectX > rectCont.x
                                                                            && centRectY < rectCont.y + rectCont.height
                                                                            && centRectY > rectCont.y)
                                                                    {
                                                                        // And then update its position.
#if ARFF
                                                                        cout << individualHeight << "," << normals << "," << circularity << "," << areaHead << ",NO,\t(" << centroidx << " , " << centroidy << ")" << endl;
#endif
                                                                        peopleCurrentFrame.addPerson(centroidx, centroidy, 
                                                                                rectCont.x + (rectCont.width / 2.), rectCont.y + (rectCont.height / 2.),
                                                                                individualHeight, areaHead, areaRect, circularity, normals, tUp, tDown, tLeft, tRight);
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                            // If not, this is a new person
                                                            else
                                                            {
#if ARFF
                                                                cout << individualHeight << "," << normals << "," << circularity << "," << areaHead << ",NO,\t(" << centroidx << " , " << centroidy << ")" << endl;
#endif
                                                                peopleCurrentFrame.addPerson(centroidx, centroidy, 0, 0, individualHeight, 
                                                                        areaHead, 0, circularity, normals, tUp, tDown, tLeft, tRight);
                                                            }  
                                                        }
                                                        isHead = false;
                                                        noHeads++;
                                                    }
                                                }
                                                // If it's out of the ROI, we simply mark it as a non head.
                                                else
                                                {
                                                    isHead = false;
                                                    noHeads++;
                                                }
                                            }

                                            if (noHeads == nCont)
                                            {
                                                allDetected = true;
                                            }

                                            // Release memory reserved for contour detection.
                                            cvReleaseMemStorage(&storageHead);
                                        }
                                        // If the blob is out of the ROI, we have finished with the contour.
                                        else
                                        {
                                            allDetected = true;
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // We draw the trajectories and update the list.
                    people.update(peopleCurrentFrame);
                    people.drawTrajectories(imageRGB, 0, 255, 0);
#if DEBUG
                    people.drawTrajectories(headsBinary, 122, 122, 122);
#endif
                    // Show the head.
                    imshow("Only heads", headsBinary);
                    cvWaitKey(1);

                    // Show the RGB image.
                    imshow("RGB", imageRGB);
                    cvWaitKey(1);

                    // Saving image to disk (THIS IS FOR THE PRESENTATION)
                    char nameImgToSave[2 * FILE_NAME_LENGTH];
                    sprintf(nameImgToSave, "/media/Data/Proyecto/ImagenesVideo/");
                    
                    char *barEnd = strrchr(filesRGB[nframe], '/');
                    char *end = strrchr(filesRGB[nframe], '\0');

                    strncat(nameImgToSave, barEnd + 1, end - 1);
                    
                    imwrite(nameImgToSave, imageRGB);
                    
#if DEBUG           
                    // Show the foreground mask.
                    imshow("FG", 255*maskFG);
                    cvWaitKey(1);

                    // Show the image with the isolated blobs.
                    imshow("Blobs", 255*maskBlob);
                    cvWaitKey(1);

                    // Show foreground cloud.
                    if (!viewerFGCreated)
                    {
                        viewerFG = rgbVis(cloudFG);
                        viewerFGCreated = true;
                    }
                    else
                    {
                        viewerFG->removeShape("sample cloud");
                        viewerFG->resetStoppedFlag();
                        rgbVis(cloudFG, viewerFG);
                    }
                    
                    while(!viewerFG->wasStopped())
                    {
                        viewerFG->spinOnce(100);
                        cvWaitKey(1);
                    }
#endif

                    // Release memory used for contours.
                    cvReleaseMemStorage(&storage);
                }
            
                // In case we do not have something to process, we still have to
                // update the list with the people with one more frame.
                // people.nextFrame() is called internally by people.update().
                else
                {
                    people.nextFrame();
                }
            }
            
#if DEBUG
            people.howMany();
#endif

            // Release memory.
            cvReleaseMemStorage(&storageMask);

            // Updating last frame mask.
            lastFrame = binaryMask;

            ++nframe;
        }   
        // End processing for each fotogram.
        people.howMany();
    }       
    // End of the image processing.

    return;
}
