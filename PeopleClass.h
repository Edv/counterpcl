/// 
/// @file        PeopleClass.h
/// @author      Eduardo Quesada Díaz <edvqd90@gmail.com>
/// @version     1.0
/// 
/// @section     DESCRIPTION 
/// Class that stores the information about the people detected by CounterPCL.
///

#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <opencv/cv.h>

#ifndef PEOPLECLASS_H
#define	PEOPLECLASS_H

///
/// \def Tolerance for the identification of the person with new data.
///
#define DELTA           0.05
///
/// \def The number of frames considered until a person (or obstacle)
/// is considered part of the background.
///
#define NUM_FRAMES      50

using namespace std;

class PeopleClass 
{
    
private:
    
    ///
    /// \struct Values
    /// This struct stores information of every instance of a person.
    ///
    struct Values
    {
        int x, y;                       ///< We have storage for the position of the person,
        int height;                     ///< the height,
        int areaHead;                   ///< the area of the head,
        int areaOverlap;                ///< the area of the overlapping with the last frame,
        double circularity;             ///< values for circularity,
        double normals;                 ///< our value for the head normal vectors,
        bool tUp, tDown, tLeft, tRight; ///< and if is touching the ends of the ROI.
    };
    
    ///
    /// \struct Person
    /// Struct that stores the info of the person through multiple instances.
    ///
    struct Person
    {
        Values info[5]; ///< Info on the last 5 positions.
        int avgHeight;  ///< Average height.
        /// 
        /// \var Parameters of the line calculated using least squares with the positions
        /// of the person; y = slope*x + constant.
        ///
        double slope, constant;
        int speed;      ///< Speed of the trajectory (mean diference between y coordinates).
        char enter;     ///< Side of the ROI where it entered (U = up, D = down, L = left, R = right)
        int framesLastUpdated;  ///< Stores how many frames have passed since this person last appeared.
        bool updated;   ///< True if this person was updated on this frame.
        int nFrames;    ///< Number of frames the person has been in the counter's field of view.
    };

    ///
    /// \var List of people stored through iterations. When a person hasn't been
    /// detected in the counter for five frames, it is deleted from this list.
    ///
    vector<Person> list;
    ///
    /// \var Stores the number of people that have passed through the counter.
    ///
    int in, out;
    
public:
    ///
    /// Constructor of the class. Only initializes counters.
    ///
    PeopleClass();
    ///
    /// Destructor of the class.
    ///
    virtual ~PeopleClass();
    
    ///
    /// Add a new person with its position and data on the list.
    ///
    /// @param x The x position of the person on the current frame.
    /// @param y The y position of the person on the current frame.
    /// @param xLast The x position of the person on the previous frame.
    /// @param yLast The y position of the person on the previous frame.
    /// @param height The calculated height of the person.
    /// @param areaHead The number of pixels corresponding to the person's head.
    /// @param areaOverlap The number of pixels of overlapping between the head
    /// of the same person on the current frame and the one on the last frame.
    /// @param circularity Calculated value for the head's circularity.
    /// @param normals Percentage of normals of the head whose bigger component is Z.
    /// @param tUp Shows if the head is touching the upper end of the ROI.
    /// @param tDown Shows if the head is touching the lower end of the ROI.
    /// @param tLeft Shows if the head is touching the left end of the ROI.
    /// @param tRight Shows if the head is touching the right end of the ROI.
    ///
    void addPerson(int x, int y, int xLast, int yLast, int height, int areaHead,
        int areaOverlap, double circularity, double normals, 
        bool tUp, bool tDown, bool tLeft, bool tRight);
    ///
    /// Sets all updated flags to zero and calculates trajectories for each person,
    /// in order to process a new frame.
    ///
    void nextFrame();
    ///
    /// Prints on console the number of people on the list.
    ///
    int howMany();
    ///
    /// Draws the trajectory of all the people currently on the list.
    ///
    /// @param img The image to draw the trajectotires into.
    /// @param r The red component of the color used to draw the trajectories.
    /// @param g The green component of the color used to draw the trajectories.
    /// @param b The blue component of the color used to draw the trajectories.
    ///
    void drawTrajectories(cv::Mat img, int r, int g, int b);
    ///
    /// Update one list with the information of the another one.
    ///
    /// @param lastFrame Information of the people to be updated on the complete list.
    ///
    void update(PeopleClass& lastFrame);
    ///
    /// Procedure to copy the info of a list on the current one.
    ///
    /// @param original Original list to be copied.
    ///
    void copy(vector<Person>& original);
};

#endif	/* PEOPLECLASS_H */

