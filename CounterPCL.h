/// 
/// @file        CounterPCL.h
/// @author      Eduardo Quesada Díaz <edvqd90@gmail.com>
/// @version     1.0
/// 
/// @section     DESCRIPTION 
/// Class that implements a person counter using data from a Kinect camera
/// using a zenital view.
///

// OpenCV includes
#include <opencv/cv.h>
#include <opencv/cvaux.h>
#include <opencv/cxcore.h>
#include <opencv/highgui.h>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/photo/photo.hpp>


// C++ includes
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <cmath>

// PCL includes
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/impl/point_types.hpp>
#include <pcl/ModelCoefficients.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/features/normal_3d.h>             // For normal calculation
#include <pcl/visualization/pcl_visualizer.h>   // Requires VTK 5.8
#include <pcl/filters/random_sample.h>
#include <pcl/search/search.h>
#include <pcl/search/kdtree.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/segmentation/region_growing_rgb.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/segmentation/sac_segmentation.h>  // ???
#include <pcl/segmentation/extract_clusters.h>  // ???
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/io/pcd_io.h>
#include <pcl/common/time.h>
#include <pcl/common/common.h>
#include <pcl/common/geometry.h>
#include <pcl/common/angles.h>
#include <pcl/common/transforms.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/features/integral_image_normal.h>
#include <pcl/segmentation/organized_multi_plane_segmentation.h>
#include <pcl/segmentation/planar_polygon_fusion.h>
#include <pcl/common/transforms.h>
#include <pcl/segmentation/plane_coefficient_comparator.h>
#include <pcl/segmentation/euclidean_plane_coefficient_comparator.h>
#include <pcl/segmentation/rgb_plane_coefficient_comparator.h>
#include <pcl/segmentation/edge_aware_plane_comparator.h>
#include <pcl/segmentation/euclidean_cluster_comparator.h>
#include <pcl/segmentation/organized_connected_component_segmentation.h>
#include <pcl/segmentation/extract_polygonal_prism_data.h>
#include <pcl/surface/convex_hull.h>

// Libfreenect include
#include "libfreenect.h"

// Project includes
#include "PeopleClass.h"

#ifndef COUNTERPCL_H
#define	COUNTERPCL_H

// Defines

///
/// \def Shows debug messages.
///
#define DEBUG                   false
///
/// \def Prints data in semi-ARFF format.
///
#define ARFF                    false
///
/// \def Minimum number of non zero pixels on the foreground to check the image.
///
#define MIN_FG_PIXELS           10000
///
/// \def Number of frames used for the background modellation.
///
#define FRAMES_BG_MODEL         200
///
/// \def Maximum number of files used on the counter.
///
#define FILES_PER_SESSION       8500
///
/// \def Length of the file names.
///
#define FILE_NAME_LENGTH        128
///
/// \def Determines if planar refinement is used on plane segmentations.
///
#define USE_PLANAR_REFINEMENT   true
///
/// \def Percentage of a person's height considered to be the head.
///
#define HEAD_HEIGHT             0.95
///
/// \def Percentage of the image we don't consider as ROI in the margins.
///
#define PERCENTAGE_ROI          0.15
///
/// \def Percentage of the image we consider as a transition area from inside or outside of the ROI.
///
#define MARGIN_ROI              0.25
///
/// \def Weight of the normals on the head decision equation.
///
#define NORMALS_WEIGHT          0.65
///
/// \def Value considered as threshold for the head decision equation.
///
#define OK_VALUE                0.7
///
/// \def Minimun value for the normals value on the head decision equation.
///
#define MIN_NORMAL_VALUE        0.6
///
/// \def Minimun value for the circularity value on the head decision equation.
///
#define MIN_CIRC_VALUE          0.6
///
/// \def Calculated value for 4*pi.
///
#define FOUR_PI                 4.f * M_PI
///
/// \def Kernel size for opening procedures.
///
#define KERNEL_SIZE             12

class CounterPCL 
{
    
public:
    
    ///
    /// Starts the people counter.
    ///
    /// @param kinect Determines if Kinect is providing the images directly,
    /// or if the program have the images on a folder.
    /// @param folder In case Kinect is not providing the images, the program 
    /// will use the ones on this folder.
    ///
    void LaunchCounter(bool kinect = true, char* filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-04-26-Start.txt");

};

#endif	/* COUNTERPCL_H */

