# Environment 
#MKDIR=mkdir
CC=g++
WLEVEL=-Wall
#CCADMIN=CCadmin

VTK_INCLUDE=/usr/include/vtk-5.8/
CFLAGS= $(WLEVEL) -march=native -O3 `pkg-config --cflags opencv` `pkg-config --cflags pcl_io-1.7` `pkg-config --cflags pcl_visualization-1.7` -I$(VTK_INCLUDE) -ffast-math -fpermissive

LDFLAGS  = `pkg-config --libs opencv`
LDFLAGS += `pkg-config --libs pcl_io-1.7` -l boost_system  
LDFLAGS += `pkg-config --libs flann`
LDFLAGS += `pkg-config --libs pcl_geometry-1.7`
LDFLAGS += `pkg-config --libs pcl_segmentation-1.7`
LDFLAGS += `pkg-config --libs pcl_visualization-1.7`
LDFLAGS += `pkg-config --libs pcl_surface-1.7`
LDFLAGS += -lvtkCommon -lvtkFiltering -l vtkRendering -lvtkGraphics

#INCLUDE = -I/usr/local/include/libfreenect
#FREE_LIBS = -L/usr/local/lib -lfreenect

SOURCES = main.cpp

all: counterpcl
	
clean:
	rm -rf *.o counterpcl *~ *.bak
	
counterpcl: CounterPCL.o PeopleClass.o main.o
	$(CC) -o $@ CounterPCL.o PeopleClass.o main.o $(LDFLAGS)
	
CounterPCL.o: CounterPCL.h CounterPCL.cpp
	$(CC) $(CFLAGS) -c CounterPCL.cpp 
	
main.o:	
	$(CC) $(CFLAGS) -c main.cpp 

PeopleClass.o: PeopleClass.h PeopleClass.cpp
	$(CC) $(CFLAGS) -c PeopleClass.cpp
	
# Old class for grabbing images from Kinect. This one used libfreenect.	
#KinectHandler.o: KinectHandler.h KinectHandler.cpp
#	$(CC) $(CFLAGS) -c KinectHandler.cpp