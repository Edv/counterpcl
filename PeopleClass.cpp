/// 
/// @file        PeopleClass.h
/// @author      Eduardo Quesada Díaz <edvqd90@gmail.com>
/// @version     1.0
/// 
/// @section     DESCRIPTION 
/// Implementation for the class that stores the information 
/// about the people detected by CounterPCL.
///

#include "PeopleClass.h"

using namespace cv;
using namespace std;

///
/// Constructor of the class. Only initializes counters.
///
PeopleClass::PeopleClass() 
{
    // Initializing counters.
    in = 0;
    out = 0;
}

///
/// Destructor of the class.
///
PeopleClass::~PeopleClass() 
{
    list.clear();
}

///
/// Add a new person with its position and data on the list.
///
/// @param x The x position of the person on the current frame.
/// @param y The y position of the person on the current frame.
/// @param xLast The x position of the person on the previous frame.
/// @param yLast The y position of the person on the previous frame.
/// @param height The calculated height of the person.
/// @param areaHead The number of pixels corresponding to the person's head.
/// @param areaOverlap The number of pixels of overlapping between the head
/// of the same person on the current frame and the one on the last frame.
/// @param circularity Calculated value for the head's circularity.
/// @param normals Percentage of normals of the head whose bigger component is Z.
/// @param tUp Shows if the head is touching the upper end of the ROI.
/// @param tDown Shows if the head is touching the lower end of the ROI.
/// @param tLeft Shows if the head is touching the left end of the ROI.
/// @param tRight Shows if the head is touching the right end of the ROI.
///
void PeopleClass::addPerson(int x, int y, int xLast, int yLast, int height,
        int areaHead, int areaOverlap, double circularity, double normals, 
        bool tUp, bool tDown, bool tLeft, bool tRight)
{
    // We create the new person.
    Person newPerson;
    newPerson.info[0].x = x;
    newPerson.info[0].y = y;
    newPerson.info[1].x = xLast;
    newPerson.info[1].y = yLast;
    for (int i = 2; i < 5; ++i)
    {
        newPerson.info[i].x = 0;
        newPerson.info[i].y = 0;
    }
    newPerson.info[0].height    = height;
    newPerson.info[0].areaHead  = areaHead;
    newPerson.info[0].areaOverlap = areaOverlap;
    newPerson.info[0].circularity = circularity;
    newPerson.info[0].normals   = normals;
    newPerson.info[0].tUp       = tUp;
    newPerson.info[0].tDown     = tDown;
    newPerson.info[0].tLeft     = tLeft;
    newPerson.info[0].tRight    = tRight;
    
    // We store where the person entered into the ROI.
    if (tUp)
    {
        newPerson.enter = 'U';
    }
    else
    {
        if (tDown)
        {
            newPerson.enter = 'D';
        }
        else
        {
            if (tLeft)
            {
                newPerson.enter = 'L';
            }
            else
            {
                if (tRight)
                {
                    newPerson.enter = 'R';
                }
                else
                {
                    newPerson.enter = 'E';
                }
            }
        }
    }
    
    newPerson.framesLastUpdated = 0;
    newPerson.speed = 0;
    newPerson.slope = 0;
    newPerson.constant = 0;
    newPerson.updated = true;
    newPerson.nFrames = 0;
    
    list.push_back(newPerson);
}

///
/// Sets all updated flags to zero and calculates trajectories for each person,
/// in order to process a new frame.
///
void PeopleClass::nextFrame()
{
    // We cycle through all the people checking when they were last updated.
    if (!list.empty())
    {
        vector<Person>::iterator i = list.begin();
    
        while (i < list.end())
        {
            // If the person has been updated this frame, we refresh all the
            // general statistics, and prepare the flags for the next frame.
            if ((*i).updated)
            {
                // Calculating coefficients for the least squares method.
                int j = 0, xi = 0, yi = 0, xiyi = 0, xi2 = 0;
                (*i).avgHeight = 0;
                (*i).speed = 0;
                while (j < 5 && (*i).info[j].x != 0)
                {
                    xi += (*i).info[j].x;
                    yi += (*i).info[j].y;
                    xiyi += ((*i).info[j].x * (*i).info[j].y);
                    xi2 += ((*i).info[j].x * (*i).info[j].x);
                    // We will also calculate the mean of other parameters.
                    (*i).avgHeight += (*i).info[j].height;
                    if (j > 0)
                    {
                        (*i).speed += (*i).info[j].y - (*i).info[j-1].y;
                    }
                    ++j;
                }
                // Number of elements = j+1.
                ++j;
                // If we have at least two positions, we calculate the line.
                if (j > 0)
                {
                    (*i).slope = (double)(j * xiyi - xi * yi) / (double)(j * xi2 - xi * xi);
                    (*i).constant = (double)(yi - (*i).slope * xi) / (double)j;
                    (*i).avgHeight /= j;
                    if (j > 1)
                    {
                        (*i).speed /= (j - 1);
                    }
                }
                
                
                (*i).framesLastUpdated = 0;
                (*i).updated = false;
                ++((*i).nFrames);
                ++i;
            }
            else
            {
                // If it has more than one position, we check if
                // the person is in or out.
                if ((*i).info[1].x != 0)
                {
                    // If on the last frame the person was touching the 
                    // limits of the ROI and it's not its first frame, 
                    // we assume it has exited.
                    if ((*i).info[0].tUp && (*i).enter != 'U')
                    {
                        ++out;
                        i = list.erase(i);
                        continue;
                    }
                    if (((*i).info[0].tDown || (*i).info[0].tLeft || (*i).info[0].tRight) 
                            && (*i).enter == 'U')
                    {
                        ++in;
                        i = list.erase(i);
                        continue;
                    }
                }
                
                // If we have processed 5 frames since the last time
                // this person was updated, we eliminate it.
                ++((*i).framesLastUpdated);
                if ((*i).framesLastUpdated >= 5)
                {
                    if (!(*i).nFrames > 1)
                    {
                        // But first we check if it probably got in or out.
                        if ((*i).info[0].y > (*i).info[1].y)
                        {
                            ++in;
                        }
                        else
                        {
                            ++out;
                        }
                    }
                    i = list.erase(i);
                    continue;
                }
                else
                {
                    ++i;
                }
            }
        }
    }
}

///
/// Prints on console the number of people on the list.
///
int PeopleClass::howMany()
{
    cout << "IN: " << in << "\tOUT: " << out << endl;
    return list.size();
}

///
/// Draws the trajectory of all the people currently on the list.
///
/// @param img The image to draw the trajectotires into.
/// @param r The red component of the color used to draw the trajectories.
/// @param g The green component of the color used to draw the trajectories.
/// @param b The blue component of the color used to draw the trajectories.
///
void PeopleClass::drawTrajectories(cv::Mat img, int r, int g, int b)
{
    // Iterates through all the people and draws lines 
    // between each point in its trajectory.
    vector<Person>::iterator i;
    for (i = list.begin(); i < list.end(); ++i)
    {
        // If the person has been on the counter for too many frames, 
        // it won't be shown (considered part of the background).
        if ((*i).updated && (*i).nFrames < NUM_FRAMES)
        {
            circle(img, Point((*i).info[0].x, (*i).info[0].y), 4, Scalar(b, g, r), -1, 8, 0);
            for (int j = 1; j < 5; ++j)
            {
                if ((*i).info[j].x != 0 &&
                        (*i).info[j].y != 0)
                {
                    line(img, Point((*i).info[j-1].x, (*i).info[j-1].y),
                            Point((*i).info[j].x, (*i).info[j].y), Scalar(b, g, r));
                }
            }
        }
    }
    
    // Calls nextFrame to prepare the list for the next iteration.
    nextFrame();
    
    // Shows on the image how many people have gone into the room 
    // and how many have come out of it.
    char text[16];
    sprintf(text, "In: %i", in);
    putText(img, std::string(text), Point(10, img.rows -16), FONT_HERSHEY_SIMPLEX, 1, Scalar(b, g, r), 2, 8, false);
    sprintf(text, "Out: %i", out);
    putText(img, std::string(text), Point(10, 34), FONT_HERSHEY_SIMPLEX, 1, Scalar(b, g, r), 2, 8, false);
}

///
/// Update one list with the information of the another one.
///
/// @param lastFrame Information of the people to be updated on the complete list.
///
void PeopleClass::update(PeopleClass& lastFrame)
{
    PeopleClass listCopy;
    listCopy.copy(list);
    vector<Person>::iterator i, j, k;
    
    for (i = lastFrame.list.begin(); i < lastFrame.list.end(); ++i)
    {
        // If the list is not empty, we check which person to update/insert.
        if (!listCopy.list.empty())
        {
            bool updated = false;
            for (j = list.begin(), k = listCopy.list.begin(); k < listCopy.list.end(); ++j, ++k)
            {
                // The person is identified by its last position.
                if (((*k).info[0].x == (*i).info[1].x) &&
                    ((*k).info[0].y == (*i).info[1].y))
                {
                    updated = true;
                    // If it's not been updated, we update its data.
                    if (!(*j).updated)
                    {
                        for (int n = 4; n > 0; --n)
                        {
                            (*j).info[n].x =        (*j).info[n-1].x;
                            (*j).info[n].y =        (*j).info[n-1].y;
                            (*j).info[n].height =   (*j).info[n-1].height;
                            (*j).info[n].areaHead = (*j).info[n-1].areaHead;
                            (*j).info[n].areaOverlap = (*j).info[n-1].areaOverlap;
                            (*j).info[n].circularity = (*j).info[n-1].circularity;
                            (*j).info[n].normals =   (*j).info[n-1].normals;
                            (*j).info[n].tUp    = (*j).info[n-1].tUp;
                            (*j).info[n].tDown  = (*j).info[n-1].tDown;
                            (*j).info[n].tLeft  = (*j).info[n-1].tLeft;
                            (*j).info[n].tRight = (*j).info[n-1].tRight;
                        }
                        (*j).info[0].x =           (*i).info[0].x;
                        (*j).info[0].y =           (*i).info[0].y;
                        (*j).info[0].height =      (*i).info[0].height;
                        (*j).info[0].areaHead =    (*i).info[0].areaHead;
                        (*j).info[0].areaOverlap = (*i).info[0].areaOverlap;
                        (*j).info[0].circularity = (*i).info[0].circularity;
                        (*j).info[0].normals =     (*i).info[0].normals;
                        (*j).info[0].tUp =         (*i).info[0].tUp;
                        (*j).info[0].tDown =       (*i).info[0].tDown;
                        (*j).info[0].tLeft =       (*i).info[0].tLeft;
                        (*j).info[0].tRight =      (*i).info[0].tRight;

                        (*j).updated = true;
                    }
                    // If it's been updated, we check if the overlap area is bigger
                    // and if it is, we update the data but only for the last frame.
                    else
                    {
                        if ((*j).info[0].areaOverlap < (*i).info[0].areaOverlap)
                        {
                            (*j).info[0].x =           (*i).info[0].x;
                            (*j).info[0].y =           (*i).info[0].y;
                            (*j).info[0].height =      (*i).info[0].height;
                            (*j).info[0].areaHead =    (*i).info[0].areaHead;
                            (*j).info[0].areaOverlap = (*i).info[0].areaOverlap;
                            (*j).info[0].circularity = (*i).info[0].circularity;
                            (*j).info[0].normals =     (*i).info[0].normals;
                            (*j).info[0].tUp =         (*i).info[0].tUp;
                            (*j).info[0].tDown =       (*i).info[0].tDown;
                            (*j).info[0].tLeft =       (*i).info[0].tLeft;
                            (*j).info[0].tRight =      (*i).info[0].tRight;
                        }
                    }
                }
            }
            
            if (!updated)
            {
                // If we didn't find a person with the same position, it's a new or lost one
                // We first check if this head belongs to a lost person.
                vector<Person>::iterator mod;
                double distMod = std::numeric_limits<double>::max();
                // We check which head that has not been updated already 
                // can calculate the nearest projection to this head.
                for (j = list.begin(), k = listCopy.list.begin(); k < listCopy.list.end(); ++j, ++k)
                {
                    if (!(*j).updated)
                    {
                        // We calculate the new position and a (rather large) area
                        // in which the head's centroid can be found.
                        int y = (*k).info[0].y - (*k).speed * ((*k).framesLastUpdated + 1);     
                        int x = (int)(((double)y - (*k).constant) / (*k).slope);
                        double r = 2 * sqrt((double)(*k).info[0].areaHead / M_PI);
                        double dist = sqrt((((*i).info[0].x-x)*((*i).info[0].x-x)) + (((*i).info[0].y-y)*((*i).info[0].y-y)));

                        // If it's in range, or if it's not but its a similar head
                        // (people that moves very fast), we update the head found.
                        if ((((*k).speed != 0) &&
                              (*i).info[0].x > x - r && (*i).info[0].x < x + r &&
                              (*i).info[0].y > y - r && (*i).info[0].y < y + r ) ||
                             ((*k).speed == 0 &&
                              (*i).info[0].circularity > ((*k).info[0].circularity - ((*k).info[0].circularity * DELTA)) &&
                              (*i).info[0].circularity < ((*k).info[0].circularity + ((*k).info[0].circularity * DELTA)) &&
                              (*i).info[0].normals > ((*k).info[0].normals - ((*k).info[0].normals * DELTA)) &&
                              (*i).info[0].normals < ((*k).info[0].normals + ((*k).info[0].normals * DELTA)) &&
                              (*i).info[0].height > ((*k).info[0].height - ((*k).info[0].height * DELTA)) &&
                              (*i).info[0].height < ((*k).info[0].height + ((*k).info[0].height * DELTA))) &&
                            dist < distMod)
                        {
                            distMod = dist;
                            mod = j;
                        }
                    }
                }
                // If there is a match, we add the head to the trajectory.
                if (distMod != std::numeric_limits<double>::max())
                {
                    for (int n = 4; n > 0; --n)
                    {
                        (*mod).info[n].x =        (*mod).info[n-1].x;
                        (*mod).info[n].y =        (*mod).info[n-1].y;
                        (*mod).info[n].height =   (*mod).info[n-1].height;
                        (*mod).info[n].areaHead = (*mod).info[n-1].areaHead;
                        (*mod).info[n].areaOverlap = (*mod).info[n-1].areaOverlap;
                        (*mod).info[n].circularity = (*mod).info[n-1].circularity;
                        (*mod).info[n].normals =   (*mod).info[n-1].normals;
                        (*mod).info[n].tUp    = (*mod).info[n-1].tUp;
                        (*mod).info[n].tDown  = (*mod).info[n-1].tDown;
                        (*mod).info[n].tLeft  = (*mod).info[n-1].tLeft;
                        (*mod).info[n].tRight = (*mod).info[n-1].tRight;
                    }
                    (*mod).info[0].x =           (*i).info[0].x;
                    (*mod).info[0].y =           (*i).info[0].y;
                    (*mod).info[0].height =      (*i).info[0].height;
                    (*mod).info[0].areaHead =    (*i).info[0].areaHead;
                    (*mod).info[0].areaOverlap = (*i).info[0].areaOverlap;
                    (*mod).info[0].circularity = (*i).info[0].circularity;
                    (*mod).info[0].normals =     (*i).info[0].normals;
                    (*mod).info[0].tUp =         (*i).info[0].tUp;
                    (*mod).info[0].tDown =       (*i).info[0].tDown;
                    (*mod).info[0].tLeft =       (*i).info[0].tLeft;
                    (*mod).info[0].tRight =      (*i).info[0].tRight;

                    (*mod).updated = true;
                }
                // If there isn't, it's a new person.
                else
                {
                    addPerson((*i).info[0].x,
                        (*i).info[0].y,
                        (*i).info[1].x,
                        (*i).info[1].y,
                        (*i).info[0].height,
                        (*i).info[0].areaHead,
                        (*i).info[0].areaOverlap,
                        (*i).info[0].circularity,
                        (*i).info[0].normals,
                        (*i).info[0].tUp,
                        (*i).info[0].tDown,
                        (*i).info[0].tLeft,
                        (*i).info[0].tRight);
                }
            }
        }
        // However, if the list is empty, we insert anyway.
        else
        {
            addPerson((*i).info[0].x,
                (*i).info[0].y,
                (*i).info[1].x,
                (*i).info[1].y,
                (*i).info[0].height,
                (*i).info[0].areaHead,
                (*i).info[0].areaOverlap,
                (*i).info[0].circularity,
                (*i).info[0].normals,
                (*i).info[0].tUp,
                (*i).info[0].tDown,
                (*i).info[0].tLeft,
                (*i).info[0].tRight);
        }
    }
}

///
/// Procedure to copy the info of a list on the current one.
///
/// @param original Original list to be copied.
///
void PeopleClass::copy(vector<Person>& original)
{
    list.clear();
    
    // We iterate through each person on the original list and
    // copy it into the current one.
    vector<Person>::iterator i;
    for (i = original.begin(); i < original.end(); ++i)
    {
        Person newPerson;
        for (int j = 0; j < 5; ++j)
        {
            newPerson.info[j].x         = (*i).info[j].x;
            newPerson.info[j].y         = (*i).info[j].y;
            newPerson.info[j].height    = (*i).info[j].height;
            newPerson.info[j].areaHead  = (*i).info[j].areaHead;
            newPerson.info[j].areaOverlap = (*i).info[j].areaOverlap;
            newPerson.info[j].circularity = (*i).info[j].circularity;
            newPerson.info[j].normals   = (*i).info[j].normals;
            newPerson.info[j].tUp       = (*i).info[j].tUp;
            newPerson.info[j].tDown     = (*i).info[j].tDown;
            newPerson.info[j].tLeft     = (*i).info[j].tLeft;
            newPerson.info[j].tRight    = (*i).info[j].tRight;
        }
        newPerson.slope = (*i).slope;
        newPerson.constant = (*i).constant;
        newPerson.avgHeight = (*i).avgHeight;
        newPerson.framesLastUpdated = (*i).framesLastUpdated;
        newPerson.speed = (*i).speed;
        newPerson.enter = (*i).enter;
        newPerson.updated = (*i).updated;
        
        list.push_back(newPerson);
    }    
}

