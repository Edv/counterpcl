#include <cstdlib>

#include "CounterPCL.h"

using namespace std;

int main(int argc, char** argv) 
{
    CounterPCL counter;
    //char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-04-19-Start.txt";
    //char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-04-19-End.txt";
    char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-04-26-Start.txt"; // Original
    //char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-04-26-End.txt";
    //char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-05-10-Start.txt";
    //char * filelist = "/media/Data/Proyecto/KinectQuimica/sesion-2013-05-10-End.txt";
    
    // Reads images from "filelist".
    counter.LaunchCounter(false, filelist);
    // Reads images directly from Kinect.
    //counter.LaunchCounter();
    
    return 0;
}

